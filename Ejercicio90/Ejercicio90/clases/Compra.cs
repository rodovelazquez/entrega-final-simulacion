﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio90.clases
{
    public class Compra
    {
        public double RandomCompra { set; get; }
        public DateTime ProximoFin { set; get; }
        public TimeSpan TiempoAtencion { set; get; }
        public Estado Estado { set; get; }
        public Queue<Cliente> Clientes { set; get; } // Revisar para cuando suba la cotización
        public Cliente ClienteActual { set; get; }

        public Compra()
        {
            RandomCompra = 0;
            ProximoFin = DateTime.MinValue;
            TiempoAtencion = TimeSpan.Zero;
            Estado = Estado.Libre;
            Clientes = new Queue<Cliente>();
            ClienteActual = null;
        }

        public Compra(Compra com)
        {
            RandomCompra = com.RandomCompra;
            ProximoFin = com.ProximoFin;
            TiempoAtencion = com.TiempoAtencion;
            Estado = com.Estado;
            Clientes = com.Clientes;
            ClienteActual = com.ClienteActual;
        }

        public void NotificarClienteNuevo()
        {
            if (ClienteActual != new Cliente() && Estado == Estado.Libre)
            {
                ClienteActual = Clientes.Dequeue();
                Estado = Estado.Ocupado;
            }
        }
    }
}
