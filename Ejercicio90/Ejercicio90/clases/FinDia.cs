﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio90.clases
{
    public class FinDia
    {
        public long NumeroDia { set; get; }
        public DateTime ProximoFin { set; get; }
        public double GananciaDiaria { set; get; }
        public long ClientesSinAtender { set; get; }

        public FinDia()
        {
            NumeroDia = 0;
            ProximoFin = DateTime.MinValue;
            GananciaDiaria = 0;
            ClientesSinAtender = 0;
        }

        public FinDia(FinDia dia)
        {
            NumeroDia = dia.NumeroDia;
            ProximoFin = dia.ProximoFin;
            GananciaDiaria = dia.GananciaDiaria;
            ClientesSinAtender = dia.ClientesSinAtender;
        }
    }
}
