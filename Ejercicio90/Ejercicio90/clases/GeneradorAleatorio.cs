﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio90.clases
{
    public static class GeneradorAleatorio
    {
        private static int raiz, c, a, m;
        private static bool esModoMultiplicativo;
        public static bool estaSeteado = false;

        public static void SetearGeneradorLineal()
        {
            List<int> numerosRaiz = new List<int>() { 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 29, 31, 33, 35, 37 };
            List<int> numerosC = new List<int>() { 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73 };
            List<int> numerosG = new List<int>() { 17, 18, 19, 20, 21, 22, 23, 24 };
            List<int> numerosK = new List<int>() { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };

            int valorRaiz, valorC, valorG, valorK;

            Random random = new Random();
            int index = random.Next(1, numerosRaiz.Count);
            valorRaiz = numerosRaiz[index - 1];

            index = random.Next(1, numerosC.Count);
            valorC = numerosC[index - 1];

            index = random.Next(1, numerosG.Count);
            valorG = numerosG[index - 1];

            index = random.Next(1, numerosK.Count);
            valorK = numerosK[index - 1];

            RecibirValoresLineal(valorRaiz, valorC, valorK, valorG);
        }

        public static void RecibirValoresLineal(int raizRecibida, int cRecibido, int k, int g)
        {
            esModoMultiplicativo = false;
            raiz = raizRecibida;
            c = cRecibido;
            a = 1 + (4 * k);

            m = 1;
            for (int i = 0; i < g; i++)
            {
                m *= 2;
            }

            // Flag en true para indicar que el generador está seteado
            estaSeteado = true;
        }

        public static int ObtenerSiguienteRaiz()
        {
            if (!estaSeteado)
            {
                SetearGeneradorLineal();
            }

            int x = raiz;

            if (esModoMultiplicativo)
            {
                x = ((a) * x) % (m);
            }
            else
            {
                x = ((a) * x + c) % (m);
            }

            raiz = x;
            return x;
        }

        public static double ObtenerSiguienteAleatorio()
        {
            if (!estaSeteado)
            {
                SetearGeneradorLineal();

                for (int i = 0; i < 10; i++)
                {
                    // Proceso para desechar los primeros 10 números
                    ObtenerSiguienteRaiz();
                }
            }

            int siguienteRaiz = ObtenerSiguienteRaiz();
            double valorTruncado = (Math.Truncate(Convert.ToDouble(siguienteRaiz) / (m - 1) * 10000)) / 10000;
            return valorTruncado;
        }

        public static double Truncar4Decimales(double numero)
        {
            double num = Math.Truncate(10000 * numero) / 10000;
            return num;
        }

        public static double Truncar2Decimales(double numero)
        {
            double num = Math.Truncate(100 * numero) / 100;
            return num;
        }

        public static double TruncarNDecimales(double numero, int cantDecimales)
        {
            double res = Math.Pow(10, cantDecimales);
            double num = Math.Truncate(res * numero) / res;
            return num;
        }

        public static double UniformeAB(double a, double b)
        {
            double uniformeAB = a + ((b - a) * ObtenerSiguienteAleatorio());
            return TruncarNDecimales(uniformeAB, 4);
        }

        public static double UniformeAB(double random, double a, double b)
        {
            double uniformeAB = a + ((b - a) * random);
            return TruncarNDecimales(uniformeAB, 4);
        }

        public static double ExponencialNegativa(double media)
        {
            double exponencial = (-((double)1 / ((double)1 / (double)media)) *
                (Math.Log(1 - ObtenerSiguienteAleatorio())));
            return TruncarNDecimales(exponencial, 4);
        }

        public static double ExponencialNegativa(double random, double media)
        {
            double exponencial = (-((double)1 / ((double)1 / (double)media)) *
                (Math.Log(1 - random)));
            return TruncarNDecimales(exponencial, 4);
        }

        public static List<double> NormalBoxMuller(int media, double desviacion)
        {
            List<double> lista = new List<double>();
            double n1, n2;
            double random1 = ObtenerSiguienteAleatorio();
            double random2 = ObtenerSiguienteAleatorio();

            n1 = Math.Sqrt(-2 * Math.Log(random1)) *
                Math.Cos(2 * Math.PI * random2) * desviacion + media;
            n2 = Math.Sqrt(-2 * Math.Log(random1)) *
                Math.Sin(2 * Math.PI * random2) * desviacion + media;

            lista.Add(TruncarNDecimales(n1, 4));
            lista.Add(TruncarNDecimales(n2, 4));
            return lista;
        }

        public static List<double> NormalBoxMuller(double random1, double random2, int media, double desviacion)
        {
            List<double> lista = new List<double>();
            double n1, n2;

            n1 = Math.Sqrt(-2 * Math.Log(random1)) *
                Math.Cos(2 * Math.PI * random2) * desviacion + media;
            n2 = Math.Sqrt(-2 * Math.Log(random1)) *
                Math.Sin(2 * Math.PI * random2) * desviacion + media;

            lista.Add(TruncarNDecimales(n1, 4));
            lista.Add(TruncarNDecimales(n2, 4));
            return lista;
        }

        public static int Poisson(int media)
        {
            double p = ObtenerSiguienteAleatorio();
            int x = 0;
            double a = Math.Exp(-media);

            while (p >= a)
            {
                p *= ObtenerSiguienteAleatorio(); ;
                x += 1;
            }
            return x;
        }

        public static int Poisson(double random, int media)
        {
            double p = random;
            int x = 0;
            double a = Math.Exp(-media);

            while (p >= a)
            {
                p *= ObtenerSiguienteAleatorio(); ;
                x += 1;
            }
            return x;
        }

    }
}
