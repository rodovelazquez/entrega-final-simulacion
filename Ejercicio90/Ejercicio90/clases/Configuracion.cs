﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio90.clases
{
    public static class Configuracion
    {
        public static double MediaLlegada { get; set; }
        public static double ProbabilidadComprar { get; set; }
        public static double ProbabilidadComprarCambioCotizacion { get; set; }
        public static double ProbabilidadVender { get; set; }
        public static double ComprarA { get; set; }
        public static double ComprarB { get; set; }
        public static double VenderA { get; set; }
        public static double VenderB { get; set; }
        public static double MontoCompra { get; set; }
        public static double MontoVentaA { get; set; }
        public static double MontoVentaB { get; set; }
        public static double ReservasIniciales { get; set; }
        public static double MinimoReservas { get; set; }
        public static double MaximoReservas { get; set; }
        public static double MontoSubidaPrecioVenta { get; set; }
        public static double MontoBajaPrecioVenta { get; set; }
        public static double MontoBajaPrecioCompra { get; set; }
        public static double PrecioCompra { get; set; }
        public static double PrecioVenta { get; set; }
        public static double PrecioCompraBancoCentral { get; set; }
        public static long DuracionDia { get; set; }
    }

    public enum Estado { Libre, Ocupado }
}
