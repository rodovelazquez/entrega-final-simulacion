﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio90.clases
{
    public class VentaCaja2
    {
        public double RandomVenta { set; get; }
        public DateTime ProximoFin { set; get; }
        public TimeSpan TiempoAtencion { set; get; }
        public Estado Estado { set; get; }
        public Queue<Cliente> Clientes { set; get; } // Revisar para cuando suba la cotización
        public Cliente ClienteActual { set; get; }

        public VentaCaja2()
        {
            RandomVenta = 0;
            ProximoFin = DateTime.MinValue;
            TiempoAtencion = TimeSpan.Zero;
            Estado = Estado.Libre;
            Clientes = new Queue<Cliente>();
            ClienteActual = null;
        }

        public VentaCaja2(VentaCaja2 ven)
        {
            RandomVenta = ven.RandomVenta;
            ProximoFin = ven.ProximoFin;
            TiempoAtencion = ven.TiempoAtencion;
            Estado = ven.Estado;
            Clientes = ven.Clientes;
            ClienteActual = ven.ClienteActual;
        }

        public void NotificarClienteNuevo()
        {
            if (ClienteActual != new Cliente() && Estado == Estado.Libre)
            {
                ClienteActual = Clientes.Dequeue();
                Estado = Estado.Ocupado;
            }
        }
    }
}
