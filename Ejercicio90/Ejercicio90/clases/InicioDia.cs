﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio90.clases
{
    public class InicioDia
    {
        public long NumeroDia { set; get; }
        public DateTime ProximoInicio { set; get; }

        public InicioDia()
        {
            NumeroDia = 0;
            ProximoInicio = new DateTime(2020, 2, 12, 8, 0, 0);
        }

        public InicioDia(InicioDia dia)
        {
            NumeroDia = dia.NumeroDia;
            ProximoInicio = dia.ProximoInicio;
        }
    }
}
