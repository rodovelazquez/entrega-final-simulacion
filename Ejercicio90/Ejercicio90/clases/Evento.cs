﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio90.clases
{
    public class Evento
    {
        public long NumeroEvento { set; get; }
        public TipoEvento Tipo { set; get; }
        public DateTime Reloj { set; get; }
        public Llegada Llegada { set; get; }
        public Compra Compra { set; get; }
        public VentaCaja1 VentaCaja1 { set; get; }
        public VentaCaja2 VentaCaja2 { set; get; }
        public InicioDia InicioDia { set; get; }
        public FinDia FinDia { set; get; }
        public double Reservas { set; get; }
        public double PrecioVenta { set; get; }
        public double PrecioCompra { set; get; }
        public bool PocasReservas { set; get; }
        public bool ReservasDeMas { set; get; }
        public int NumeroHoraReservasDeMas { set; get; }
        public DateTime HoraReservasDeMas { get; set; }
        public long ClientesSinAtender { set; get; }
        public double AcumuladoReservas { set; get; }
        public double AcumuladoClientesSinAtender { set; get; }
        public double ReservasApertura { set; get; }
        public double PesosInicioDia { set; get; }

        public Evento()
        {
            NumeroEvento = 0;
            Tipo = TipoEvento.Inicio;
            Reloj = new DateTime(2020, 12, 2, 7, 59, 59);

            Llegada = new Llegada();
            Compra = new Compra();
            VentaCaja1 = new VentaCaja1();
            VentaCaja2 = new VentaCaja2();
            InicioDia = new InicioDia();
            FinDia = new FinDia();

            Reservas = Configuracion.ReservasIniciales;
            PrecioVenta = Configuracion.PrecioVenta;
            PrecioCompra = Configuracion.PrecioCompra;
            PocasReservas = false;
            ReservasDeMas = false;
            NumeroHoraReservasDeMas = -1;
            HoraReservasDeMas = DateTime.MinValue;
            ClientesSinAtender = 0;
            AcumuladoClientesSinAtender = 0;
            AcumuladoReservas = 0;
            ReservasApertura = Configuracion.ReservasIniciales;
            PesosInicioDia = 0;
        }

        public Evento(Evento eve)
        {
            NumeroEvento = eve.NumeroEvento;
            Tipo = eve.Tipo;
            Reloj = eve.Reloj;

            Llegada = eve.Llegada;
            Compra = eve.Compra;
            VentaCaja1 = eve.VentaCaja1;
            VentaCaja2 = eve.VentaCaja2;
            InicioDia = eve.InicioDia;
            FinDia = eve.FinDia;

            Reservas = eve.Reservas;
            PrecioVenta = eve.PrecioVenta;
            PrecioCompra = eve.PrecioCompra;
            PocasReservas = eve.PocasReservas;
            ReservasDeMas = eve.ReservasDeMas;
            NumeroHoraReservasDeMas = eve.NumeroHoraReservasDeMas;
            HoraReservasDeMas = eve.HoraReservasDeMas;
            ClientesSinAtender = eve.ClientesSinAtender;
            AcumuladoReservas = eve.AcumuladoReservas;
            AcumuladoClientesSinAtender = eve.AcumuladoClientesSinAtender;
            ReservasApertura = eve.ReservasApertura;
            PesosInicioDia = eve.PesosInicioDia;
        }
    }

    public enum TipoEvento
    {
        Inicio, Llegada, FinCompra, FinVentaCaja1, FinVentaCaja2, InicioDia, FinDia
    }
}
