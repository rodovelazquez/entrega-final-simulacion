﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio90.clases
{
    public class Llegada
    {
        public double RandomLlegada { get; set; }
        public TimeSpan TiempoEntreLlegadas { get; set; }
        public DateTime ProximaLlegada { get; set; }
        public Cliente Cliente { get; set; }

        public Llegada()
        {
            RandomLlegada = 0;
            TiempoEntreLlegadas = TimeSpan.Zero;
            ProximaLlegada = DateTime.MinValue;
            Cliente = new Cliente();
        }

        public Llegada(Llegada lleg)
        {
            RandomLlegada = lleg.RandomLlegada;
            TiempoEntreLlegadas = lleg.TiempoEntreLlegadas;
            ProximaLlegada = lleg.ProximaLlegada;
            Cliente = lleg.Cliente;
        }
    }
}
