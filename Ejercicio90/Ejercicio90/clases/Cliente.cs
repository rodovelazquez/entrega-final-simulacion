﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio90.clases
{
    public class Cliente
    {
        public long NumeroCliente { get; set; }
        public double RandomAccion { get; set; }
        public Accion Accion { get; set; }
        public double RandomMonto { get; set; }
        public double Monto { get; set; }

        public Cliente()
        {
            NumeroCliente = 0;
            RandomAccion = 0;
            Accion = Accion.Null;
            RandomMonto = 0;
            Monto = 0;
        }

        public Cliente(Cliente cli)
        {
            NumeroCliente = cli.NumeroCliente;
            RandomAccion = cli.RandomAccion;
            Accion = cli.Accion;
            RandomMonto = cli.RandomMonto;
            Monto = cli.Monto;
        }
    }

    public enum Accion
    {
        Compra, Venta, Retiro, Null
    }
}
