﻿using Ejercicio90.clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio90
{
    public partial class FormPrincipal : Form
    {
        private long diasSimular; // 900 iteraciones por dia
        private long diaActual;
        private long cantidadIteraciones;
        private long iteracion;
        private long contadorClientes;
        private long clientesQueSeRetiran;
        private double promedioClientesSinAtender;
        private double promedioGananciaDiaria;
        private double reservasBanco;
        private double dineroBancoPesos;

        private double reservasApertura;
        private double precioVentaApertura;
        private double precioCompraApertura;
        private bool genteSeRetira;
        private double diaDesde;
        private double diaHasta;
        private double acumuladorGanancia;

        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {
            ConfiguracionInicial();
            ConfigurarDataGrid();
        }

        private void ButtonSimular_Click(object sender, EventArgs e)
        {
            bool puedeContinuar = LeerValoresTextBoxes();
            if (!puedeContinuar)
            {
                return;
            }
            IniciarSimulacion();
        }

        private bool LeerValoresTextBoxes()
        {
            if (checkBoxValoresDefecto.Checked)
            {
                diasSimular = Convert.ToInt64(textBoxCantidadDias.Text);
                diaDesde = Convert.ToDouble(textBoxDiaDesde.Text);
                diaHasta = Convert.ToDouble(textBoxDiaHasta.Text);
                return true;
            }
            bool puedeContinuar = VerificarValoresNoNegativos();
            if (!puedeContinuar)
            {
                MessageBox.Show("Error", "Revisar valores ingresados antes de continuar",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            CargarConfiguraciones();
            return puedeContinuar;
        }

        private void CargarConfiguraciones()
        {
            diasSimular = Convert.ToInt64(textBoxCantidadDias.Text);
            diaDesde = Convert.ToDouble(textBoxDiaDesde.Text);
            diaHasta = Convert.ToDouble(textBoxDiaHasta.Text);

            Configuracion.MediaLlegada = Convert.ToDouble(textBoxMediaLlegadas.Text);
            Configuracion.ProbabilidadComprar = Convert.ToDouble(textBoxProbCompra.Text);
            Configuracion.ProbabilidadVender = Convert.ToDouble(textBoxProbVenta.Text);
            Configuracion.ComprarA = Convert.ToDouble(textBoxCajaCompraA.Text);
            Configuracion.ComprarB = Convert.ToDouble(textBoxCajaCompraB.Text);
            Configuracion.VenderA = Convert.ToDouble(textBoxCajaVentaA.Text);
            Configuracion.VenderB = Convert.ToDouble(textBoxCajaVentaB.Text);
            Configuracion.MontoCompra = Convert.ToDouble(textBoxMontoCompra.Text);
            Configuracion.MontoVentaA = Convert.ToDouble(textBoxMontoVentaA.Text);
            Configuracion.MontoVentaB = Convert.ToDouble(textBoxMontoVentaB.Text);
            Configuracion.PrecioCompra = Convert.ToDouble(textBoxPrecioCompra.Text);
            Configuracion.PrecioVenta = Convert.ToDouble(textBoxPrecioVenta.Text);
            Configuracion.PrecioCompraBancoCentral = Convert.ToDouble(textBoxPrecioCompraBC.Text);
            Configuracion.ProbabilidadComprarCambioCotizacion = Configuracion.ProbabilidadComprar / 2;
        }

        private void ConfiguracionInicial()
        {
            Configuracion.DuracionDia = 8;
            Configuracion.MediaLlegada = 1; //1
            Configuracion.ProbabilidadComprar = 0.6;
            Configuracion.ProbabilidadVender = 0.4;
            Configuracion.ProbabilidadComprarCambioCotizacion = Configuracion.ProbabilidadComprar / 2;

            Configuracion.ComprarA = 1;
            Configuracion.ComprarB = 2;
            Configuracion.VenderA = 0.5;
            Configuracion.VenderB = 1.5;

            Configuracion.MontoCompra = 500; //500
            Configuracion.MontoVentaA = 500;
            Configuracion.MontoVentaB = 1000; //1000

            Configuracion.ReservasIniciales = 4000;
            Configuracion.MinimoReservas = 4000 * 0.5;
            Configuracion.MaximoReservas = 4000 * 1.25;

            Configuracion.MontoSubidaPrecioVenta = 0.1;
            Configuracion.MontoBajaPrecioVenta = 0.05;
            Configuracion.MontoBajaPrecioCompra = 0.05;
            Configuracion.PrecioCompra = 3.35;
            Configuracion.PrecioVenta = 3.45;
            Configuracion.PrecioCompraBancoCentral = 3.4;
        }

        private void ConfigurarDataGrid()
        {
            dataGridViewResultados.ColumnCount = 31;
            dataGridViewResultados.Columns[0].HeaderText = "Iterac.";
            dataGridViewResultados.Columns[1].HeaderText = "Evento";
            dataGridViewResultados.Columns[2].HeaderText = "Reloj";
            dataGridViewResultados.Columns[3].HeaderText = "Random Llegada";
            dataGridViewResultados.Columns[4].HeaderText = "Tiempo entre Llegadas";
            dataGridViewResultados.Columns[5].HeaderText = "Próxima Llegada";
            dataGridViewResultados.Columns[6].HeaderText = "Random Acción";
            dataGridViewResultados.Columns[7].HeaderText = "Acción";
            dataGridViewResultados.Columns[8].HeaderText = "Random Fin Venta Caja 1";
            dataGridViewResultados.Columns[9].HeaderText = "Tiempo Atención Caja 1";
            dataGridViewResultados.Columns[10].HeaderText = "Próximo Fin Caja 1";
            dataGridViewResultados.Columns[11].HeaderText = "Random Fin Venta Caja 2";
            dataGridViewResultados.Columns[12].HeaderText = "Tiempo Atención Caja 2";
            dataGridViewResultados.Columns[13].HeaderText = "Próximo Fin Caja 2";
            dataGridViewResultados.Columns[14].HeaderText = "Random Fin Compra";
            dataGridViewResultados.Columns[15].HeaderText = "Tiempo Atención Compra";
            dataGridViewResultados.Columns[16].HeaderText = "Próximo Fin Compra";
            dataGridViewResultados.Columns[17].HeaderText = "Random Monto";
            dataGridViewResultados.Columns[18].HeaderText = "Monto";
            dataGridViewResultados.Columns[19].HeaderText = "Cola Venta 1";
            dataGridViewResultados.Columns[20].HeaderText = "Cola Venta 2";
            dataGridViewResultados.Columns[21].HeaderText = "Cola Compa";
            dataGridViewResultados.Columns[22].HeaderText = "Estado Venta 1";
            dataGridViewResultados.Columns[23].HeaderText = "Estado Venta 2";
            dataGridViewResultados.Columns[24].HeaderText = "Estado Compa";
            dataGridViewResultados.Columns[25].HeaderText = "Reservas Dólares";
            dataGridViewResultados.Columns[26].HeaderText = "Precio Venta";
            dataGridViewResultados.Columns[27].HeaderText = "Precio Compa";
            dataGridViewResultados.Columns[28].HeaderText = "Acumulador Pesos Ganados";
            dataGridViewResultados.Columns[29].HeaderText = "Ganancia diaria promedio";
            dataGridViewResultados.Columns[30].HeaderText = "Gente que se retiró";

            dataGridViewResultados.Columns[0].Width = 50;
            dataGridViewResultados.Columns[1].Width = 85;
            dataGridViewResultados.Columns[2].Width = 110;
            dataGridViewResultados.Columns[3].Width = 55;
            dataGridViewResultados.Columns[4].Width = 60;
            dataGridViewResultados.Columns[5].Width = 110;
            dataGridViewResultados.Columns[6].Width = 55;
            dataGridViewResultados.Columns[7].Width = 50;
            dataGridViewResultados.Columns[8].Width = 55;
            dataGridViewResultados.Columns[9].Width = 60;
            dataGridViewResultados.Columns[10].Width = 110;
            dataGridViewResultados.Columns[11].Width = 55;
            dataGridViewResultados.Columns[12].Width = 60;
            dataGridViewResultados.Columns[13].Width = 110;
            dataGridViewResultados.Columns[14].Width = 55;
            dataGridViewResultados.Columns[15].Width = 60;
            dataGridViewResultados.Columns[16].Width = 110;
            dataGridViewResultados.Columns[17].Width = 55;
            dataGridViewResultados.Columns[18].Width = 50;
            dataGridViewResultados.Columns[19].Width = 50;
            dataGridViewResultados.Columns[20].Width = 50;
            dataGridViewResultados.Columns[21].Width = 50;
            dataGridViewResultados.Columns[22].Width = 60;
            dataGridViewResultados.Columns[23].Width = 60;
            dataGridViewResultados.Columns[24].Width = 60;
            dataGridViewResultados.Columns[25].Width = 60;
            dataGridViewResultados.Columns[26].Width = 50;
            dataGridViewResultados.Columns[27].Width = 50;
            dataGridViewResultados.Columns[28].Width = 70;
            dataGridViewResultados.Columns[29].Width = 70;
            dataGridViewResultados.Columns[30].Width = 70;
        }

        private void PonerContadoresEnCero()
        {
            diaActual = 0;
            iteracion = 0;
        }

        private void IniciarSimulacion()
        {
            dataGridViewResultados.Rows.Clear();
            PonerContadoresEnCero();
            contadorClientes = 0;
            genteSeRetira = false;

            Evento anterior = new Evento();
            EventoInicio(anterior);
            anterior.Compra.ClienteActual = new Cliente();
            MostrarEvento(anterior); 
            ReseteoColumnas(anterior);

            diaActual = 1;
            iteracion = 1;
            progressBarSimulacion.Maximum = (int) diasSimular;

            while (diaActual < diasSimular)
            {
                Evento actual = new Evento(anterior);
                actual.NumeroEvento++;
                actual.Reloj = DeterminarTiempoReloj(anterior); // Muevo el reloj al evento que toca
                actual.Tipo = DeterminarTipoEvento(anterior, actual.Reloj);

                switch (actual.Tipo)
                {
                    case TipoEvento.Llegada:
                        EventoLlegada(actual, anterior);
                        break;
                    case TipoEvento.FinCompra:
                        EventoFinCompra(actual, anterior);
                        break;
                    case TipoEvento.FinVentaCaja1:
                        EventoFinVentaCaja1(actual, anterior);
                        break;
                    case TipoEvento.FinVentaCaja2:
                        EventoFinVentaCaja2(actual, anterior);
                        break;
                    case TipoEvento.InicioDia:
                        EventoInicioDia(actual, anterior);
                        break;
                    case TipoEvento.FinDia:
                        EventoFinDia(actual, anterior);
                        actual.ClientesSinAtender = 0;
                        anterior = actual;
                        MostrarEvento(actual); // Se muestran todos los finDia

                        actual = new Evento();
                        actual.NumeroEvento = anterior.NumeroEvento;
                        actual.Reloj = actual.Reloj.AddDays(diaActual);
                        actual.InicioDia.ProximoInicio = anterior.InicioDia.ProximoInicio;
                        actual.FinDia.ProximoFin = anterior.FinDia.ProximoFin;

                        actual.Reservas = anterior.Reservas;
                        actual.ReservasApertura = actual.Reservas;
                        actual.PrecioVenta = anterior.PrecioVenta;
                        precioVentaApertura = anterior.PrecioVenta;
                        actual.PrecioCompra = anterior.PrecioCompra;
                        precioCompraApertura = anterior.PrecioCompra;
                        reservasApertura = anterior.Reservas;

                        if (actual.Reservas < anterior.ReservasApertura * 0.5)
                        {
                            actual.PocasReservas = true;
                        }
                        if (actual.Reservas > anterior.ReservasApertura * 1.25)
                        {
                            actual.ReservasDeMas = true;
                        }

                        actual.AcumuladoClientesSinAtender = anterior.AcumuladoClientesSinAtender;
                        actual.AcumuladoReservas = anterior.Reservas;
                        actual.Compra.ClienteActual = new Cliente();

                        diaActual++; // Se previene el loop
                        break;

                    default:
                        break;
                }

                if (diaActual <= 5) // entre diaDesde y diaHasta
                {
                    if (actual.Tipo != TipoEvento.Inicio) { MostrarEvento(actual); } // Solo si corresponde
                }

                progressBarSimulacion.Value = (int) diaActual;
                ReseteoColumnas(actual);
                anterior = actual;
                iteracion++;
            }

            PintarCeldas();
        }

        private TimeSpan RoundTimeSpan(int precision, TimeSpan ts)
        {
            const int timespanSize = 7;
            int factor = (int)Math.Pow(10, (timespanSize - precision));

            TimeSpan roundedTimeSpan = new TimeSpan(((long)Math.Round((1.0 * ts.Ticks / factor)) * factor));

            return roundedTimeSpan;
        }

        private void MostrarEvento(Evento eve)
        {
            string proximaLlegada = eve.Llegada.ProximaLlegada.ToString();
            if (eve.Tipo == TipoEvento.Inicio)
            {
                proximaLlegada = "";
            }
            string accion = eve.Llegada.Cliente.Accion.ToString();
            if (diaActual > diaHasta) { VerificarEvento(eve); }
            string randomAccion = eve.Llegada.Cliente.RandomAccion.ToString();
            if (eve.Tipo != TipoEvento.Llegada)
            {
                accion = "";
                randomAccion = "";
            }

            string randomLlegada = eve.Llegada.RandomLlegada.ToString();
            string randomVentaCaja1 = eve.VentaCaja1.RandomVenta.ToString();
            string randomVentaCaja2 = eve.VentaCaja2.RandomVenta.ToString();
            string randomCompra = eve.Compra.RandomCompra.ToString();
            string randomMonto = eve.Compra.ClienteActual.RandomMonto.ToString();
            if (randomLlegada == "0")
            {
                randomLlegada = "";
            }
            if (randomVentaCaja1 == "0")
            {
                randomVentaCaja1 = "";
            }
            if (randomVentaCaja2 == "0")
            {
                randomVentaCaja2 = "";
            }
            if (randomCompra == "0")
            {
                randomCompra = "";
            }
            if (randomMonto == "0")
            {
                randomMonto = "";
            }

            string finVenta1 = eve.VentaCaja1.ProximoFin.ToString();
            string finVenta2 = eve.VentaCaja2.ProximoFin.ToString();
            string finCompra = eve.Compra.ProximoFin.ToString();
            if (eve.VentaCaja1.ProximoFin == DateTime.MinValue)
            {
                finVenta1 = "";
            }
            if (eve.VentaCaja2.ProximoFin == DateTime.MinValue)
            {
                finVenta2 = "";
            }
            if (eve.Compra.ProximoFin == DateTime.MinValue)
            {
                finCompra = "";
            }

            string tiempoEntreLlegadas = eve.Llegada.TiempoEntreLlegadas.ToString();
            string tiempoAtencionVenta1 = eve.VentaCaja1.TiempoAtencion.ToString();
            string tiempoAtencionVenta2 = eve.VentaCaja2.TiempoAtencion.ToString();
            string tiempoAtencionCompra = eve.Compra.TiempoAtencion.ToString();
            if (tiempoEntreLlegadas == "00:00:00")
            {
                tiempoEntreLlegadas = "";
            }
            if (tiempoAtencionVenta1 == "00:00:00")
            {
                tiempoAtencionVenta1 = "";
            }
            if (tiempoAtencionVenta2 == "00:00:00")
            {
                tiempoAtencionVenta2 = "";
            }
            if (tiempoAtencionCompra == "00:00:00")
            {
                tiempoAtencionCompra = "";
            }

            dataGridViewResultados.Rows.Add(iteracion, eve.Tipo, eve.Reloj, randomLlegada, tiempoEntreLlegadas,
                proximaLlegada, randomAccion, accion, randomVentaCaja1, tiempoAtencionVenta1, finVenta1,
                randomVentaCaja2, tiempoAtencionVenta2, finVenta2, randomCompra, tiempoAtencionCompra, finCompra,
                randomMonto, eve.Compra.ClienteActual.Monto, eve.VentaCaja1.Clientes.Count,
                eve.VentaCaja2.Clientes.Count, eve.Compra.Clientes.Count, eve.VentaCaja1.Estado, eve.VentaCaja2.Estado,
                eve.Compra.Estado, reservasBanco, GeneradorAleatorio.Truncar2Decimales(eve.PrecioVenta),
                GeneradorAleatorio.Truncar2Decimales(eve.PrecioCompra), GeneradorAleatorio.Truncar2Decimales(acumuladorGanancia),
                GeneradorAleatorio.Truncar2Decimales(promedioGananciaDiaria), clientesQueSeRetiran);
        }

        private void ReseteoColumnas(Evento eve)
        {
            eve.Llegada.RandomLlegada = 0;
            eve.Llegada.TiempoEntreLlegadas = TimeSpan.Zero;

            eve.Compra.RandomCompra = 0;
            eve.Compra.TiempoAtencion = TimeSpan.Zero;

            eve.VentaCaja1.RandomVenta = 0;
            eve.VentaCaja1.TiempoAtencion = TimeSpan.Zero;

            eve.VentaCaja2.RandomVenta = 0;
            eve.VentaCaja2.TiempoAtencion = TimeSpan.Zero;
        }

        private DateTime DeterminarTiempoReloj(Evento eve)
        {
            List<DateTime> lista = new List<DateTime>
            {
                eve.InicioDia.ProximoInicio,
                eve.Llegada.ProximaLlegada,
                eve.Compra.ProximoFin,
                eve.VentaCaja1.ProximoFin,
                eve.VentaCaja2.ProximoFin,
                eve.FinDia.ProximoFin
            };

            DateTime fechaMinima = lista.Min();
            while (fechaMinima < eve.Reloj)
            {
                lista.Remove(fechaMinima);
                lista.Min(record => record);
                fechaMinima = lista.Min();
            };

            if (fechaMinima == DateTime.MinValue) throw new Exception("El reloj NO debería tener una fecha máxima.");

            return fechaMinima;
        }

        private TipoEvento DeterminarTipoEvento(Evento eve, DateTime Reloj)
        {
            DateTime masProximo = Reloj;

            if (eve.Llegada.ProximaLlegada == masProximo) return TipoEvento.Llegada;
            else if (eve.Compra.ProximoFin == masProximo) return TipoEvento.FinCompra;
            else if (eve.VentaCaja1.ProximoFin == masProximo) return TipoEvento.FinVentaCaja1;
            else if (eve.VentaCaja2.ProximoFin == masProximo) return TipoEvento.FinVentaCaja2;
            else if (eve.FinDia.ProximoFin == masProximo) return TipoEvento.FinDia;
            else return TipoEvento.InicioDia;

        }

        private void EventoInicio(Evento eve)
        {
            reservasBanco = Configuracion.ReservasIniciales;
            eve.Reservas = Configuracion.ReservasIniciales;
            dineroBancoPesos = reservasBanco * Configuracion.PrecioCompraBancoCentral;
            acumuladorGanancia = dineroBancoPesos;
            eve.PesosInicioDia = dineroBancoPesos;
            DateTime hora = eve.Reloj.AddSeconds(1);
            eve.InicioDia.ProximoInicio = hora; // 08:00 am

            precioCompraApertura = Configuracion.PrecioCompra;
            precioVentaApertura = Configuracion.PrecioVenta;
            reservasApertura = Configuracion.ReservasIniciales;
            eve.ReservasApertura = reservasApertura;
        }

        private void EventoInicioDia(Evento actual, Evento anterior)
        {
            DateTime fecha = actual.InicioDia.ProximoInicio;
            fecha = fecha.AddDays(1);
            actual.InicioDia.ProximoInicio = fecha;

            double random = GeneradorAleatorio.ObtenerSiguienteAleatorio();
            actual.Llegada.RandomLlegada = random;

            double tiempo = GeneradorAleatorio.ExponencialNegativa(random, Configuracion.MediaLlegada);
            while (Double.IsInfinity(tiempo))
            {
                random = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                tiempo = GeneradorAleatorio.ExponencialNegativa(random, Configuracion.MediaLlegada);
            }
            actual.PesosInicioDia = dineroBancoPesos;
            actual.Llegada.ProximaLlegada = actual.Reloj.AddMinutes(tiempo);
            DateTime dt = actual.Llegada.ProximaLlegada;
            actual.Llegada.TiempoEntreLlegadas = RoundTimeSpan(0, actual.Llegada.ProximaLlegada - actual.Reloj);
            actual.FinDia.ProximoFin = actual.Reloj.AddHours(Configuracion.DuracionDia);
        }

        private void EventoLlegada(Evento actual, Evento anterior)
        {
            contadorClientes++; // Porque llega un cliente
            RevisarCambioCotizacion(anterior, actual);
            double random = GeneradorAleatorio.ObtenerSiguienteAleatorio();
            if (random < 0.03) // Esto evita que haya 2 llegadas en el mismo instante
            {
                random = GeneradorAleatorio.ObtenerSiguienteAleatorio();
            }
            actual.Llegada.RandomLlegada = random;

            // Cálculo de la próxima llegada
            double tiempo = GeneradorAleatorio.ExponencialNegativa(random, Configuracion.MediaLlegada);
            while (Double.IsInfinity(tiempo)) {
                random = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                tiempo = GeneradorAleatorio.ExponencialNegativa(random, Configuracion.MediaLlegada);
            }
            actual.Llegada.ProximaLlegada = actual.Reloj.AddMinutes(tiempo);
            actual.Llegada.TiempoEntreLlegadas = RoundTimeSpan(0, actual.Llegada.ProximaLlegada - actual.Reloj);

            // Cálculo del Cliente
            actual.Llegada.Cliente = new Cliente(); //Esto es lo que no referencia permanentemente al mismo cliente
            actual.Llegada.Cliente.NumeroCliente = contadorClientes;
            double otroRandom = GeneradorAleatorio.ObtenerSiguienteAleatorio();
            actual.Llegada.Cliente.RandomAccion = otroRandom;
            actual.Llegada.Cliente.Accion = DeterminarAccionCliente(actual, otroRandom);
            if (actual.Llegada.Cliente.Accion == Accion.Retiro)
            {
                actual.ClientesSinAtender++;
                clientesQueSeRetiran++;
                return;
            }

            // Asignar Cliente a cola, en base al evento vemos que le toca hacer
            if (actual.Llegada.Cliente.Accion == Accion.Compra)
            {
                if (DebeIrACajaVenta1(anterior))
                {
                    actual.VentaCaja1.Clientes.Enqueue(actual.Llegada.Cliente);
                    actual.VentaCaja1.NotificarClienteNuevo();

                    if (actual.Llegada.Cliente == actual.VentaCaja1.ClienteActual)
                    {
                        // calculo su proximo fin porque la caja está desocupada
                        double rand = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                        actual.VentaCaja1.RandomVenta = rand;
                        double temp = GeneradorAleatorio.UniformeAB(rand, Configuracion.VenderA, Configuracion.VenderB);
                        while (Double.IsInfinity(temp))
                        {
                            rand = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                            temp = GeneradorAleatorio.ExponencialNegativa(rand, Configuracion.MediaLlegada);
                        }
                        actual.VentaCaja1.ProximoFin = actual.Reloj.AddMinutes(temp);
                        actual.VentaCaja1.TiempoAtencion = RoundTimeSpan(0, actual.VentaCaja1.ProximoFin - actual.Reloj);

                        int monto = Int32.Parse(Configuracion.MontoCompra.ToString());
                        actual.VentaCaja1.ClienteActual.Monto = monto;
                    }
                }
                else
                {
                    actual.VentaCaja2.Clientes.Enqueue(actual.Llegada.Cliente);
                    actual.VentaCaja2.NotificarClienteNuevo();

                    if (actual.Llegada.Cliente == actual.VentaCaja2.ClienteActual)
                    {
                        double rand = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                        actual.VentaCaja2.RandomVenta = rand;
                        double temp = GeneradorAleatorio.UniformeAB(rand, Configuracion.VenderA, Configuracion.VenderB);
                        while (Double.IsInfinity(temp))
                        {
                            rand = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                            temp = GeneradorAleatorio.ExponencialNegativa(rand, Configuracion.MediaLlegada);
                        }
                        actual.VentaCaja2.ProximoFin = actual.Reloj.AddMinutes(temp);
                        actual.VentaCaja2.TiempoAtencion = RoundTimeSpan(0, actual.VentaCaja2.ProximoFin - actual.Reloj);

                        int monto = Int32.Parse(Configuracion.MontoCompra.ToString());
                        actual.VentaCaja2.ClienteActual.Monto = monto;
                    }
                }
            }
            else
            {
                actual.Compra.Clientes.Enqueue(actual.Llegada.Cliente);
                actual.Compra.NotificarClienteNuevo();

                if (actual.Llegada.Cliente == actual.Compra.ClienteActual)
                {
                    double rand = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                    actual.Compra.RandomCompra = rand;
                    double temp = GeneradorAleatorio.UniformeAB(rand, Configuracion.ComprarA, Configuracion.ComprarB);
                    while (Double.IsInfinity(temp))
                    {
                        rand = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                        temp = GeneradorAleatorio.ExponencialNegativa(rand, Configuracion.MediaLlegada);
                    }
                    actual.Compra.ProximoFin = actual.Reloj.AddMinutes(temp);
                    actual.Compra.TiempoAtencion = RoundTimeSpan(0, actual.Compra.ProximoFin - actual.Reloj);

                    double rnd = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                    actual.Compra.ClienteActual.RandomMonto = rnd;
                    int monto = Convert.ToInt32(GeneradorAleatorio.UniformeAB(rnd, Configuracion.MontoVentaA, Configuracion.MontoVentaB));
                    actual.Compra.ClienteActual.Monto = monto;
                }
            }
        }

        private void EventoFinCompra(Evento actual, Evento anterior)
        {
            Cliente cli = actual.Compra.ClienteActual;
            reservasBanco += cli.Monto;
            actual.Reservas += cli.Monto;
            dineroBancoPesos -= cli.Monto * actual.PrecioCompra;
            RevisarCambioCotizacion(actual, anterior); // hay cambio en la cotizacion?

            if (actual.Compra.Clientes.Count > 0)
            {
                // Ahora lo relacionado con la próxima compra
                actual.Compra.ClienteActual = actual.Compra.Clientes.Dequeue();
                actual.Compra.Estado = Estado.Ocupado;

                // Cálculo del proximoFinCompra (porque hay gente en la cola)
                double random = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                actual.Compra.RandomCompra = random;
                double tiempo = GeneradorAleatorio.UniformeAB(random, Configuracion.ComprarA, Configuracion.ComprarB);
                while (Double.IsInfinity(tiempo))
                {
                    random = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                    tiempo = GeneradorAleatorio.ExponencialNegativa(random, Configuracion.MediaLlegada);
                }
                actual.Compra.ProximoFin = actual.Reloj.AddMinutes(tiempo);
                actual.Compra.TiempoAtencion = RoundTimeSpan(0, actual.Compra.ProximoFin - actual.Reloj);
                // Cálculo del monto de la compra
                double otroRandom = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                actual.Compra.ClienteActual.RandomMonto = otroRandom;
                int monto = Convert.ToInt32(GeneradorAleatorio.UniformeAB(otroRandom, Configuracion.MontoVentaA, Configuracion.MontoVentaB));
                actual.Compra.ClienteActual.Monto = monto;
            }
            else
            {
                // Por ahora no hay proximoFinCompra
                actual.Compra.ClienteActual = new Cliente();
                actual.Compra.Estado = Estado.Libre;
                actual.Compra.ProximoFin = DateTime.MinValue;
            }
        }

        private void EventoFinVentaCaja1(Evento actual, Evento anterior)
        {
            Cliente cli = actual.VentaCaja1.ClienteActual;
            reservasBanco -= cli.Monto;
            actual.Reservas -= cli.Monto;
            dineroBancoPesos += cli.Monto * actual.PrecioVenta;
            RevisarCambioCotizacion(actual, anterior); // hay cambio en la cotizacion?

            if (actual.VentaCaja1.Clientes.Count > 0)
            {
                actual.VentaCaja1.ClienteActual = actual.VentaCaja1.Clientes.Dequeue();
                actual.VentaCaja1.Estado = Estado.Ocupado;

                double random = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                actual.VentaCaja1.RandomVenta = random;
                double tiempo = GeneradorAleatorio.UniformeAB(random, Configuracion.VenderA, Configuracion.VenderB);
                while (Double.IsInfinity(tiempo))
                {
                    random = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                    tiempo = GeneradorAleatorio.ExponencialNegativa(random, Configuracion.MediaLlegada);
                }
                actual.VentaCaja1.ProximoFin = actual.Reloj.AddMinutes(tiempo);
                actual.VentaCaja1.TiempoAtencion = RoundTimeSpan(0, actual.VentaCaja1.ProximoFin - actual.Reloj);

                int monto = Int32.Parse(Configuracion.MontoCompra.ToString());
                actual.VentaCaja1.ClienteActual.Monto = monto;
            }
            else
            {
                // Por ahora no hay proximoFinVenta
                actual.VentaCaja1.ClienteActual = new Cliente();
                actual.VentaCaja1.Estado = Estado.Libre;
                actual.VentaCaja1.ProximoFin = DateTime.MinValue;
            }
        }

        private void EventoFinVentaCaja2(Evento actual, Evento anterior)
        {
            Cliente cli = actual.VentaCaja2.ClienteActual;
            reservasBanco -= cli.Monto;
            actual.Reservas -= cli.Monto;
            dineroBancoPesos += cli.Monto * actual.PrecioVenta;
            RevisarCambioCotizacion(actual, anterior); // hay cambio en la cotizacion?

            if (actual.VentaCaja2.Clientes.Count > 0)
            {
                actual.VentaCaja2.ClienteActual = actual.VentaCaja2.Clientes.Dequeue();
                actual.VentaCaja2.Estado = Estado.Ocupado;

                double random = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                actual.VentaCaja2.RandomVenta = random;
                double tiempo = GeneradorAleatorio.UniformeAB(random, Configuracion.VenderA, Configuracion.VenderB);
                while (Double.IsInfinity(tiempo))
                {
                    random = GeneradorAleatorio.ObtenerSiguienteAleatorio();
                    tiempo = GeneradorAleatorio.ExponencialNegativa(random, Configuracion.MediaLlegada);
                }
                actual.VentaCaja2.ProximoFin = actual.Reloj.AddMinutes(tiempo);
                actual.VentaCaja2.TiempoAtencion = RoundTimeSpan(0, actual.VentaCaja2.ProximoFin - actual.Reloj);

                int monto = Int32.Parse(Configuracion.MontoCompra.ToString());
                actual.VentaCaja2.ClienteActual.Monto = monto;
            }
            else
            {
                // Por ahora no hay proximoFinVenta
                actual.VentaCaja2.ClienteActual = new Cliente();
                actual.VentaCaja2.Estado = Estado.Libre;
                actual.VentaCaja2.ProximoFin = DateTime.MinValue;
            }

        }

        private void EventoFinDia(Evento actual, Evento anterior)
        {
            actual.AcumuladoClientesSinAtender += actual.ClientesSinAtender;
            double promedioClientes = actual.AcumuladoClientesSinAtender / diaActual;
            promedioClientes = GeneradorAleatorio.Truncar2Decimales(promedioClientes);
            labelPromedioClientesSinAtender.Text = promedioClientes.ToString();

            actual.AcumuladoReservas += actual.Reservas;
            double promedioGananciaDolares = actual.AcumuladoReservas / diaActual;
            promedioGananciaDolares = GeneradorAleatorio.Truncar2Decimales(promedioGananciaDolares);
            labelPromedioGananciaBanco.Text = promedioGananciaDolares.ToString();

            acumuladorGanancia = acumuladorGanancia + (dineroBancoPesos - actual.PesosInicioDia);
            double promedioPesos = acumuladorGanancia / diaActual;
            promedioGananciaDiaria = promedioPesos;
            promedioPesos = GeneradorAleatorio.Truncar2Decimales(promedioPesos);
            labelPromedioPesos.Text = promedioPesos.ToString();

            DateTime fecha = actual.FinDia.ProximoFin;
            fecha = fecha.AddDays(1);
            actual.FinDia.ProximoFin = fecha;
        }

        private Accion DeterminarAccionCliente(Evento eve, double random)
        {
            if (!genteSeRetira) //!eve.PocasReservas
            {
                if (random < Configuracion.ProbabilidadComprar)
                {
                    return Accion.Compra;
                }
                else
                {
                    return Accion.Venta;
                }
            }
            else
            {
                if (random < Configuracion.ProbabilidadComprarCambioCotizacion)
                {
                    return Accion.Compra;
                }
                else if (random < Configuracion.ProbabilidadComprar)
                {
                    return Accion.Retiro;
                }
                else
                {
                    return Accion.Venta;
                }
            }
        }

        private void RevisarCambioCotizacion(Evento eve, Evento anterior)
        {
            if (reservasBanco <= 0.5 * reservasApertura)
            {
                if (!eve.PocasReservas)
                {
                    eve.PocasReservas = true;
                    eve.PrecioVenta += 0.10;
                    genteSeRetira = true;
                    QuitarMitadPersonasCola(eve);
                }
            }
            else if (reservasBanco >= 1.25 * reservasApertura)
            {
                if (!eve.ReservasDeMas)
                {
                    eve.HoraReservasDeMas = eve.Reloj;
                    eve.ReservasDeMas = true;
                    eve.PrecioVenta -= 0.05;
                    if (eve.PrecioVenta <= precioVentaApertura)
                    {
                        genteSeRetira = false;
                    }
                    if (eve.PrecioVenta < Configuracion.PrecioCompraBancoCentral)
                    {
                        eve.PrecioCompra -= 0.05;
                    }
                }
                else
                {
                    genteSeRetira = false;
                    if (eve.Reloj - anterior.HoraReservasDeMas >= new TimeSpan(1, 0, 0))
                    {
                        eve.HoraReservasDeMas = eve.Reloj;
                        eve.ReservasDeMas = true;
                        eve.PrecioVenta -= 0.05;
                        if (eve.PrecioVenta <= precioVentaApertura)
                        {
                            genteSeRetira = false;
                        }
                        if (eve.PrecioVenta <= Configuracion.PrecioCompraBancoCentral)
                        {
                            eve.PrecioCompra -= 0.05;
                        }
                    }
                }
            }
            else // Reservas en orden
            {
                eve.PocasReservas = false;
                eve.ReservasDeMas = false;
            }
        }

        private void QuitarMitadPersonasCola(Evento eve)
        {
            Queue<Cliente> queue1 = new Queue<Cliente>();
            Queue<Cliente> queue2 = new Queue<Cliente>();
            int longCola1 = eve.VentaCaja1.Clientes.Count();
            int longCola2 = eve.VentaCaja2.Clientes.Count();

            for (int i = 0; i < longCola1 / 2; i++)
            {
                queue1.Enqueue(eve.VentaCaja1.Clientes.Dequeue());
            }
            for (int i = 0; i < longCola2 / 2; i++)
            {
                queue2.Enqueue(eve.VentaCaja2.Clientes.Dequeue());
            }
            if (queue1.Count > 0) { eve.VentaCaja1.Clientes = queue1; }
            if (queue2.Count > 0) { eve.VentaCaja2.Clientes = queue2; }
        }

        private bool DebeIrACajaVenta1(Evento eve)
        {
            if (eve.VentaCaja1.Estado == Estado.Libre)
            {
                return true;
            }
            if (eve.VentaCaja2.Estado == Estado.Libre)
            {
                return false;
            }
            if (eve.VentaCaja1.Clientes.Count <= eve.VentaCaja2.Clientes.Count)
            {
                return true;
            }
            return false;
        }

        private void PintarCeldas()
        {
            Color color;
            string tipo = String.Empty;
            foreach (DataGridViewRow row in dataGridViewResultados.Rows)
            {
                if (row.Cells[1].Value == null) break;
                tipo = row.Cells[1].Value.ToString();

                switch (tipo)
                {
                    case "Inicio":
                        color = Color.Plum;
                        break;
                    case "InicioDia":
                        color = Color.DarkSeaGreen;
                        break;
                    case "FinDia":
                        color = Color.LightPink;
                        break;
                    case "Llegada":
                        color = Color.LightGoldenrodYellow;
                        break;
                    case "FinCompra":
                        color = Color.PeachPuff;
                        break;
                    case "FinVentaCaja1":
                        color = Color.PowderBlue;
                        break;
                    case "FinVentaCaja2":
                        color = Color.PowderBlue;
                        break;
                    default:
                        color = Color.WhiteSmoke;
                        break;
                }

                row.DefaultCellStyle.BackColor = color;
            }
        }

        #region MetodosSinUtilizar

        private void VerificarCambioCotizacion(Evento eve, Evento anterior)
        {
            if (eve.PocasReservas)
            {
                if (reservasBanco > 1.25 * eve.ReservasApertura)
                {
                    eve.PocasReservas = false;
                    //eve.PrecioVenta = Configuracion.PrecioVenta; //Comentables
                    // eve.PrecioCompra = Configuracion.PrecioVenta - 0.05;
                }
                return;
            }
            else if (eve.ReservasDeMas)
            {
                if (eve.Reloj - anterior.HoraReservasDeMas < new TimeSpan(1, 0, 0))
                {
                    if (reservasBanco < 0.5 * eve.ReservasApertura)
                    {
                        eve.ReservasDeMas = false;
                        //eve.PrecioVenta = Configuracion.PrecioVenta; //Comentables
                        //eve.PrecioCompra = Configuracion.PrecioCompra; // REVISAR
                    }
                    return;
                }
                else
                {
                    eve.HoraReservasDeMas = eve.Reloj;
                    eve.ReservasDeMas = true;
                    eve.PrecioVenta -= 0.05;
                    if (eve.PrecioVenta <= Configuracion.PrecioCompraBancoCentral)
                    {
                        eve.PrecioCompra -= 0.05;
                    }
                    return;
                }
                /*if (eve.Reloj.Hour == anterior.NumeroHoraReservasDeMas)
                {
                    return; //Si las horas coinciden, lo hago esperar más tiempo
                }*/
            }

            if (reservasBanco <= 0.5 * eve.ReservasApertura) // && !eve.PocasReservas
            {
                // inicia el modo pocas reservas
                eve.PocasReservas = true;
                eve.PrecioVenta += 0.10;
                genteSeRetira = true;
                //QuitarMitadPersonasCola();
            }
            else if (reservasBanco >= 1.25 * eve.ReservasApertura) // && !eve.ReservasDeMas
            {
                // GUARDAR LA HORA EN QUE PASA ESTO para después verificar
                // eve.NumeroHoraReservasDeMas = eve.Reloj.Hour;
                eve.HoraReservasDeMas = eve.Reloj;
                eve.ReservasDeMas = true;
                eve.PrecioVenta -= 0.05;
                if (eve.PrecioVenta <= precioVentaApertura)
                {
                    genteSeRetira = false;
                }
                if (eve.PrecioVenta < Configuracion.PrecioCompraBancoCentral)
                {
                    eve.PrecioCompra -= 0.05;
                }
            }
            else
            {
                eve.PocasReservas = false;
                eve.ReservasDeMas = false;
                //eve.PrecioCompra = Configuracion.PrecioCompra;
                //eve.PrecioVenta = Configuracion.PrecioVenta;
            }
        }

        private void VerificarEvento(Evento eve)
        {
            List<double> lista = new List<double>() { 2.30, 2.45, 2.55, 2.60, 2.70, 2.75, 2.85, 2.90, 3,00
                , 3.10, 3.15, 3.30, 3.40, 3.55, 3.60, 3.80, 3.90, 4.05 , 4.15, 4.20, 4.30, 4.40, 4.45, 4.55
                , 4.65, 4.70, 4.80, 4.85, 4.95 };
            Random random = new Random();
            int index = random.Next(1, lista.Count);
            double precioVenta = lista[index - 1];

            List<double> menos = new List<double>() { 0.05, 0.10, 0.15, 0.20, 0.25, 0.30};
            int ind = random.Next(1, menos.Count);
            double dif = menos[ind - 1];

            eve.PrecioVenta = precioVenta;
            eve.PrecioCompra = precioVenta - dif;
        }

        private void ResetearContadores(Evento actual, Evento anterior)
        {
            actual.ClientesSinAtender = 0;
            if (actual.Reservas < Configuracion.ReservasIniciales)
            {
                int faltante = Convert.ToInt32(Configuracion.ReservasIniciales - actual.Reservas);
                actual.Reservas += faltante;
                dineroBancoPesos -= faltante * Configuracion.PrecioCompraBancoCentral;
            }

            anterior = actual;

            actual = new Evento();
            actual.NumeroEvento = anterior.NumeroEvento;
            actual.Reloj = actual.Reloj.AddDays(diaActual);
            actual.InicioDia.ProximoInicio = anterior.InicioDia.ProximoInicio;
            actual.FinDia.ProximoFin = anterior.FinDia.ProximoFin;

            actual.Reservas = anterior.Reservas;
            actual.AcumuladoClientesSinAtender = anterior.AcumuladoClientesSinAtender;
            actual.AcumuladoReservas = anterior.Reservas;
            
            // Fechas en 0
            /*actual.Llegada.ProximaLlegada = DateTime.MinValue;
            actual.Compra.ProximoFin = DateTime.MinValue;
            actual.VentaCaja1.ProximoFin = DateTime.MinValue;
            actual.VentaCaja2.ProximoFin = DateTime.MinValue;*/

            // Objetos que pueden causar problemas
            //actual.Llegada = new Llegada();

            // Contadores
            /*actual.ClientesSinAtender = 0;
            if (actual.Reservas < Configuracion.ReservasIniciales)
            {
                int faltante = Convert.ToInt32(Configuracion.ReservasIniciales - actual.Reservas);
                actual.Reservas += faltante;
                dineroBancoPesos -= faltante * Configuracion.PrecioCompraBancoCentral;
            }*/
        }

        #endregion

        private void CheckBoxValoresDefecto_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxValoresDefecto.Checked)
            {
                textBoxMediaLlegadas.Enabled = false;
                textBoxMediaLlegadas.Text = "1";
                textBoxMontoCompra.Enabled = false;
                textBoxMontoCompra.Text = "500";
                textBoxProbCompra.Enabled = false;
                textBoxProbCompra.Text = "0,6";
                textBoxProbVenta.Enabled = false;
                textBoxProbVenta.Text = "0,4";
                textBoxCajaCompraA.Enabled = false;
                textBoxCajaCompraA.Text = "1";
                textBoxCajaCompraB.Enabled = false;
                textBoxCajaCompraB.Text = "2";
                textBoxCajaVentaA.Enabled = false;
                textBoxCajaVentaA.Text = "0,5";
                textBoxCajaVentaB.Enabled = false;
                textBoxCajaVentaB.Text = "1,5";
                textBoxMontoVentaA.Enabled = false;
                textBoxMontoVentaA.Text = "500";
                textBoxMontoVentaB.Enabled = false;
                textBoxMontoVentaB.Text = "1000";
                textBoxPrecioCompra.Enabled = false;
                textBoxPrecioCompra.Text = "3,35";
                textBoxPrecioVenta.Enabled = false;
                textBoxPrecioVenta.Text = "3,45";
                textBoxPrecioCompraBC.Enabled = false;
                textBoxPrecioCompraBC.Text = "3,40";
            }
            else
            {
                textBoxMediaLlegadas.Enabled = true;
                textBoxMontoCompra.Enabled = true;
                textBoxProbCompra.Enabled = true;
                textBoxProbVenta.Enabled = true;
                textBoxCajaCompraA.Enabled = true;
                textBoxCajaCompraB.Enabled = true;
                textBoxCajaVentaA.Enabled = true;
                textBoxCajaVentaB.Enabled = true;
                textBoxMontoVentaA.Enabled = true;
                textBoxMontoVentaB.Enabled = true;
                textBoxPrecioCompra.Enabled = true;
                textBoxPrecioVenta.Enabled = true;
                textBoxPrecioCompraBC.Enabled = true;
            }
        }

        private bool VerificarValoresNoNegativos()
        {
            if (textBoxMediaLlegadas.Text == "" || textBoxMontoCompra.Text == "" || textBoxProbCompra.Text == ""
                || textBoxProbVenta.Text == "" || textBoxCajaCompraA.Text == "" || textBoxCajaCompraB.Text == ""
                || textBoxCajaVentaA.Text == "" || textBoxCajaVentaB.Text == "" || textBoxMontoVentaA.Text == ""
                || textBoxMontoVentaB.Text == "" || textBoxPrecioCompra.Text == "" || textBoxPrecioVenta.Text == ""
                || textBoxPrecioCompraBC.Text == "" || textBoxCantidadDias.Text == "" || textBoxDiaDesde.Text == ""
                || textBoxDiaHasta.Text == "")
            {
                return false;
            }

            if (textBoxMediaLlegadas.Text.Contains("-") || textBoxMontoCompra.Text.Contains("-") || textBoxProbCompra.Text.Contains("-")
                || textBoxProbVenta.Text.Contains("-") || textBoxCajaCompraA.Text.Contains("-") || textBoxCajaCompraB.Text.Contains("-")
                || textBoxCajaVentaA.Text.Contains("-") || textBoxCajaVentaB.Text.Contains("-") || textBoxMontoVentaA.Text.Contains("-")
                || textBoxMontoVentaB.Text.Contains("-") || textBoxPrecioCompra.Text.Contains("-") || textBoxPrecioVenta.Text.Contains("-")
                || textBoxPrecioCompraBC.Text.Contains("-") || textBoxCantidadDias.Text.Contains("-") || textBoxDiaDesde.Text.Contains("-")
                || textBoxDiaHasta.Text.Contains("-"))
            {
                return false;
            }

            return true;
        }
    }
}
