﻿namespace Ejercicio90
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewResultados = new System.Windows.Forms.DataGridView();
            this.buttonSimular = new System.Windows.Forms.Button();
            this.progressBarSimulacion = new System.Windows.Forms.ProgressBar();
            this.labelPromedioClientesSinAtender = new System.Windows.Forms.Label();
            this.labelPromedioGananciaBanco = new System.Windows.Forms.Label();
            this.labelPromedioPesos = new System.Windows.Forms.Label();
            this.textBoxCantidadDias = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxDiaDesde = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxDiaHasta = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxProbCompra = new System.Windows.Forms.TextBox();
            this.textBoxProbVenta = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxMediaLlegadas = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxCajaCompraA = new System.Windows.Forms.TextBox();
            this.textBoxCajaCompraB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxCajaVentaB = new System.Windows.Forms.TextBox();
            this.textBoxCajaVentaA = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxMontoCompra = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxMontoVentaA = new System.Windows.Forms.TextBox();
            this.textBoxMontoVentaB = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.checkBoxValoresDefecto = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxPrecioCompra = new System.Windows.Forms.TextBox();
            this.textBoxPrecioVenta = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxPrecioCompraBC = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResultados)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewResultados
            // 
            this.dataGridViewResultados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResultados.Location = new System.Drawing.Point(12, 143);
            this.dataGridViewResultados.Name = "dataGridViewResultados";
            this.dataGridViewResultados.Size = new System.Drawing.Size(1211, 476);
            this.dataGridViewResultados.TabIndex = 0;
            // 
            // buttonSimular
            // 
            this.buttonSimular.Location = new System.Drawing.Point(918, 112);
            this.buttonSimular.Name = "buttonSimular";
            this.buttonSimular.Size = new System.Drawing.Size(75, 23);
            this.buttonSimular.TabIndex = 1;
            this.buttonSimular.Text = "Simular";
            this.buttonSimular.UseVisualStyleBackColor = true;
            this.buttonSimular.Click += new System.EventHandler(this.ButtonSimular_Click);
            // 
            // progressBarSimulacion
            // 
            this.progressBarSimulacion.Location = new System.Drawing.Point(999, 112);
            this.progressBarSimulacion.Name = "progressBarSimulacion";
            this.progressBarSimulacion.Size = new System.Drawing.Size(224, 23);
            this.progressBarSimulacion.TabIndex = 2;
            // 
            // labelPromedioClientesSinAtender
            // 
            this.labelPromedioClientesSinAtender.AutoSize = true;
            this.labelPromedioClientesSinAtender.Location = new System.Drawing.Point(1125, 28);
            this.labelPromedioClientesSinAtender.Name = "labelPromedioClientesSinAtender";
            this.labelPromedioClientesSinAtender.Size = new System.Drawing.Size(10, 13);
            this.labelPromedioClientesSinAtender.TabIndex = 3;
            this.labelPromedioClientesSinAtender.Text = "-";
            // 
            // labelPromedioGananciaBanco
            // 
            this.labelPromedioGananciaBanco.AutoSize = true;
            this.labelPromedioGananciaBanco.Location = new System.Drawing.Point(1125, 58);
            this.labelPromedioGananciaBanco.Name = "labelPromedioGananciaBanco";
            this.labelPromedioGananciaBanco.Size = new System.Drawing.Size(10, 13);
            this.labelPromedioGananciaBanco.TabIndex = 4;
            this.labelPromedioGananciaBanco.Text = "-";
            // 
            // labelPromedioPesos
            // 
            this.labelPromedioPesos.AutoSize = true;
            this.labelPromedioPesos.Location = new System.Drawing.Point(1125, 87);
            this.labelPromedioPesos.Name = "labelPromedioPesos";
            this.labelPromedioPesos.Size = new System.Drawing.Size(10, 13);
            this.labelPromedioPesos.TabIndex = 5;
            this.labelPromedioPesos.Text = "-";
            // 
            // textBoxCantidadDias
            // 
            this.textBoxCantidadDias.Location = new System.Drawing.Point(276, 25);
            this.textBoxCantidadDias.Name = "textBoxCantidadDias";
            this.textBoxCantidadDias.Size = new System.Drawing.Size(65, 20);
            this.textBoxCantidadDias.TabIndex = 6;
            this.textBoxCantidadDias.Text = "1000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(250, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Cantidad de días a simular (900 iteraciones por día)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Mostrar desde el día";
            // 
            // textBoxDiaDesde
            // 
            this.textBoxDiaDesde.Location = new System.Drawing.Point(130, 51);
            this.textBoxDiaDesde.Name = "textBoxDiaDesde";
            this.textBoxDiaDesde.Size = new System.Drawing.Size(65, 20);
            this.textBoxDiaDesde.TabIndex = 9;
            this.textBoxDiaDesde.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(207, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "hasta el día";
            // 
            // textBoxDiaHasta
            // 
            this.textBoxDiaHasta.Location = new System.Drawing.Point(276, 51);
            this.textBoxDiaHasta.Name = "textBoxDiaHasta";
            this.textBoxDiaHasta.Size = new System.Drawing.Size(65, 20);
            this.textBoxDiaHasta.TabIndex = 11;
            this.textBoxDiaHasta.Text = "5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Probabilidad de Comprar Dólares";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Probabilidad de Vender Dólares";
            // 
            // textBoxProbCompra
            // 
            this.textBoxProbCompra.Location = new System.Drawing.Point(187, 88);
            this.textBoxProbCompra.Name = "textBoxProbCompra";
            this.textBoxProbCompra.Size = new System.Drawing.Size(50, 20);
            this.textBoxProbCompra.TabIndex = 14;
            this.textBoxProbCompra.Text = "0,6";
            // 
            // textBoxProbVenta
            // 
            this.textBoxProbVenta.Location = new System.Drawing.Point(187, 114);
            this.textBoxProbVenta.Name = "textBoxProbVenta";
            this.textBoxProbVenta.Size = new System.Drawing.Size(50, 20);
            this.textBoxProbVenta.TabIndex = 15;
            this.textBoxProbVenta.Text = "0,4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(396, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Media de Llegadas";
            // 
            // textBoxMediaLlegadas
            // 
            this.textBoxMediaLlegadas.Location = new System.Drawing.Point(499, 25);
            this.textBoxMediaLlegadas.Name = "textBoxMediaLlegadas";
            this.textBoxMediaLlegadas.Size = new System.Drawing.Size(50, 20);
            this.textBoxMediaLlegadas.TabIndex = 17;
            this.textBoxMediaLlegadas.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(272, 91);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Caja Compra A";
            // 
            // textBoxCajaCompraA
            // 
            this.textBoxCajaCompraA.Location = new System.Drawing.Point(355, 88);
            this.textBoxCajaCompraA.Name = "textBoxCajaCompraA";
            this.textBoxCajaCompraA.Size = new System.Drawing.Size(50, 20);
            this.textBoxCajaCompraA.TabIndex = 19;
            this.textBoxCajaCompraA.Text = "1";
            // 
            // textBoxCajaCompraB
            // 
            this.textBoxCajaCompraB.Location = new System.Drawing.Point(355, 114);
            this.textBoxCajaCompraB.Name = "textBoxCajaCompraB";
            this.textBoxCajaCompraB.Size = new System.Drawing.Size(50, 20);
            this.textBoxCajaCompraB.TabIndex = 20;
            this.textBoxCajaCompraB.Text = "2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(272, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Caja Compra B";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(443, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Cajas Venta B";
            // 
            // textBoxCajaVentaB
            // 
            this.textBoxCajaVentaB.Location = new System.Drawing.Point(523, 114);
            this.textBoxCajaVentaB.Name = "textBoxCajaVentaB";
            this.textBoxCajaVentaB.Size = new System.Drawing.Size(50, 20);
            this.textBoxCajaVentaB.TabIndex = 24;
            this.textBoxCajaVentaB.Text = "1,5";
            // 
            // textBoxCajaVentaA
            // 
            this.textBoxCajaVentaA.Location = new System.Drawing.Point(523, 88);
            this.textBoxCajaVentaA.Name = "textBoxCajaVentaA";
            this.textBoxCajaVentaA.Size = new System.Drawing.Size(50, 20);
            this.textBoxCajaVentaA.TabIndex = 23;
            this.textBoxCajaVentaA.Text = "0,5";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(443, 91);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Cajas Venta A";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(396, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Monto de Compra";
            // 
            // textBoxMontoCompra
            // 
            this.textBoxMontoCompra.Location = new System.Drawing.Point(499, 51);
            this.textBoxMontoCompra.Name = "textBoxMontoCompra";
            this.textBoxMontoCompra.Size = new System.Drawing.Size(50, 20);
            this.textBoxMontoCompra.TabIndex = 27;
            this.textBoxMontoCompra.Text = "500";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(595, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Monto de Venta A";
            // 
            // textBoxMontoVentaA
            // 
            this.textBoxMontoVentaA.Location = new System.Drawing.Point(694, 25);
            this.textBoxMontoVentaA.Name = "textBoxMontoVentaA";
            this.textBoxMontoVentaA.Size = new System.Drawing.Size(50, 20);
            this.textBoxMontoVentaA.TabIndex = 29;
            this.textBoxMontoVentaA.Text = "500";
            // 
            // textBoxMontoVentaB
            // 
            this.textBoxMontoVentaB.Location = new System.Drawing.Point(694, 51);
            this.textBoxMontoVentaB.Name = "textBoxMontoVentaB";
            this.textBoxMontoVentaB.Size = new System.Drawing.Size(50, 20);
            this.textBoxMontoVentaB.TabIndex = 31;
            this.textBoxMontoVentaB.Text = "1000";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(595, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(93, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "Monto de Venta B";
            // 
            // checkBoxValoresDefecto
            // 
            this.checkBoxValoresDefecto.AutoSize = true;
            this.checkBoxValoresDefecto.Location = new System.Drawing.Point(784, 32);
            this.checkBoxValoresDefecto.Name = "checkBoxValoresDefecto";
            this.checkBoxValoresDefecto.Size = new System.Drawing.Size(86, 30);
            this.checkBoxValoresDefecto.TabIndex = 32;
            this.checkBoxValoresDefecto.Text = "Usar Valores\r\n por Defecto";
            this.checkBoxValoresDefecto.UseVisualStyleBackColor = true;
            this.checkBoxValoresDefecto.CheckedChanged += new System.EventHandler(this.CheckBoxValoresDefecto_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(612, 91);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Precio Venta";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(612, 117);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 34;
            this.label15.Text = "Precio Compra";
            // 
            // textBoxPrecioCompra
            // 
            this.textBoxPrecioCompra.Location = new System.Drawing.Point(694, 114);
            this.textBoxPrecioCompra.Name = "textBoxPrecioCompra";
            this.textBoxPrecioCompra.Size = new System.Drawing.Size(50, 20);
            this.textBoxPrecioCompra.TabIndex = 36;
            this.textBoxPrecioCompra.Text = "3,35";
            // 
            // textBoxPrecioVenta
            // 
            this.textBoxPrecioVenta.Location = new System.Drawing.Point(694, 88);
            this.textBoxPrecioVenta.Name = "textBoxPrecioVenta";
            this.textBoxPrecioVenta.Size = new System.Drawing.Size(50, 20);
            this.textBoxPrecioVenta.TabIndex = 35;
            this.textBoxPrecioVenta.Text = "3,45";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(779, 91);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "Precio Compra BC";
            // 
            // textBoxPrecioCompraBC
            // 
            this.textBoxPrecioCompraBC.Location = new System.Drawing.Point(801, 107);
            this.textBoxPrecioCompraBC.Name = "textBoxPrecioCompraBC";
            this.textBoxPrecioCompraBC.Size = new System.Drawing.Size(50, 20);
            this.textBoxPrecioCompraBC.TabIndex = 38;
            this.textBoxPrecioCompraBC.Text = "3,40";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(915, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(157, 13);
            this.label17.TabIndex = 39;
            this.label17.Text = "Promedio de gente que se retira";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(915, 58);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(192, 13);
            this.label18.TabIndex = 40;
            this.label18.Text = "Promedio de reservas diarias del banco";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(915, 87);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(191, 13);
            this.label19.TabIndex = 41;
            this.label19.Text = "Promedio de ganancia diaria del banco";
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1235, 631);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.textBoxPrecioCompraBC);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBoxPrecioCompra);
            this.Controls.Add(this.textBoxPrecioVenta);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.checkBoxValoresDefecto);
            this.Controls.Add(this.textBoxMontoVentaB);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBoxMontoVentaA);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBoxMontoCompra);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxCajaVentaB);
            this.Controls.Add(this.textBoxCajaVentaA);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxCajaCompraB);
            this.Controls.Add(this.textBoxCajaCompraA);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxMediaLlegadas);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxProbVenta);
            this.Controls.Add(this.textBoxProbCompra);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxDiaHasta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxDiaDesde);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxCantidadDias);
            this.Controls.Add(this.labelPromedioPesos);
            this.Controls.Add(this.labelPromedioGananciaBanco);
            this.Controls.Add(this.labelPromedioClientesSinAtender);
            this.Controls.Add(this.progressBarSimulacion);
            this.Controls.Add(this.buttonSimular);
            this.Controls.Add(this.dataGridViewResultados);
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "FormPrincipal";
            this.Text = "[75347] Velázquez Rodolfo - Ejercicio 90 ";
            this.Load += new System.EventHandler(this.FormPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResultados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewResultados;
        private System.Windows.Forms.Button buttonSimular;
        private System.Windows.Forms.ProgressBar progressBarSimulacion;
        private System.Windows.Forms.Label labelPromedioClientesSinAtender;
        private System.Windows.Forms.Label labelPromedioGananciaBanco;
        private System.Windows.Forms.Label labelPromedioPesos;
        private System.Windows.Forms.TextBox textBoxCantidadDias;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxDiaDesde;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxDiaHasta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxProbCompra;
        private System.Windows.Forms.TextBox textBoxProbVenta;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxMediaLlegadas;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxCajaCompraA;
        private System.Windows.Forms.TextBox textBoxCajaCompraB;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxCajaVentaB;
        private System.Windows.Forms.TextBox textBoxCajaVentaA;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxMontoCompra;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxMontoVentaA;
        private System.Windows.Forms.TextBox textBoxMontoVentaB;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox checkBoxValoresDefecto;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxPrecioCompra;
        private System.Windows.Forms.TextBox textBoxPrecioVenta;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxPrecioCompraBC;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
    }
}

